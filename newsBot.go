package main

import (
	"encoding/json"
	"fmt"
	. "slack-bot/bot"
	"strings"
	"time"
)

const NEW_URL = `https://app.dailynow.co/graphql?query=query fetchLatest($params: QueryPostInput) { latest(params: $params) { id,title,url,publishedAt,createdAt,image,ratio,placeholder,views,readTime,publication { id, name, image },tags } }&variables={"params":{"latest":"%v","page":0,"pageSize":%v,"pubs":"","tags":"","sortBy":"%v"}}`
const QUOTE_URL = `https://wt-13aa445b4e3aa3eaf74205a4bf23c313-0.sandbox.auth0-extend.com/news/quote`

type News struct {
	Data struct {
		Latest []struct {
			ID          string    `json:"id"`
			Title       string    `json:"title"`
			URL         string    `json:"url"`
			PublishedAt time.Time `json:"publishedAt"`
			CreatedAt   time.Time `json:"createdAt"`
			Image       string    `json:"image"`
			Ratio       float64   `json:"ratio"`
			Placeholder string    `json:"placeholder"`
			Views       int       `json:"views"`
			ReadTime    int       `json:"readTime"`
			Publication struct {
				ID    string `json:"id"`
				Name  string `json:"name"`
				Image string `json:"image"`
			} `json:"publication"`
			Tags []string `json:"tags"`
		} `json:"latest"`
	} `json:"data"`
}

type NewQuote struct {
	Success struct {
		Total int `json:"total"`
	} `json:"success"`
	Contents struct {
		Quotes []struct {
			Quote      string            `json:"quote"`
			Length     string            `json:"length"`
			Author     string            `json:"author"`
			Tags       map[string]string `json:"tags"`
			Category   string            `json:"category"`
			Language   string            `json:"language"`
			Date       string            `json:"date"`
			Permalink  string            `json:"permalink"`
			ID         string            `json:"id"`
			Background string            `json:"background"`
			Title      string            `json:"title"`
		} `json:"quotes"`
	} `json:"contents"`
	Baseurl   string `json:"baseurl"`
	Copyright struct {
		Year int    `json:"year"`
		URL  string `json:"url"`
	} `json:"copyright"`
}

type SortBy string

const POPULAR SortBy = "popularity"
const RECENT SortBy = "creation"

func getNewUrl(sortBy SortBy, limit int) string {
	newUrl := fmt.Sprintf(NEW_URL, time.Now().Format("2006-01-02T15:04:05.0Z"), limit, sortBy)
	return strings.Replace(newUrl, " ", "%20", -1)
}

const NEW_LIMIT = 7

func getNews(msg *Message) error {
	newJson, err := get(getNewUrl(POPULAR, NEW_LIMIT))
	if err != nil {
		return err
	}
	news := new(News)
	err = json.Unmarshal(newJson, news)
	if err != nil {
		return err
	}

	msg.AddAttachment(
		Attachment().SetText(B(fmt.Sprintf("Most %v popular posts", len(news.Data.Latest)))),
	)

	for _, article := range news.Data.Latest {
		msg.AddAttachment(
			Attachment().
				SetAuthorIcon(article.Publication.Image).
				SetColor("green").
				SetAuthorName(article.Publication.Name).
				SetThumbUrl(article.Image).
				SetTitle(article.Title).
				SetTs(article.PublishedAt.Unix()).
				SetTitleLink(article.URL).
				SetFooter(If(NULL, article.ReadTime, "", fmt.Sprint(article.ReadTime, " mins read")).(string)),
		)
	}
	return nil
}

func getQuote(msg *Message) error {
	quoteJson, err := get(QUOTE_URL)
	if err != nil {
		return err
	}
	quote := new(NewQuote)
	err = json.Unmarshal(quoteJson, quote)
	if err != nil {
		return err
	}
	topQuote := quote.Contents.Quotes[0]
	msg.AddBlocks(
		ImageB().SetImage(topQuote.Background, topQuote.Title).SetTitle(topQuote.Title),
		Section().SetText(Text(MRKDWN, I(B(topQuote.Quote)))),
		Context().AddElements(
			Text(PLAIN, topQuote.Author),
		),
		Divider(),
	)

	return nil
}

func newsBot() (string, error) {

	msg := bot.Create(nil).SetChannel("test-bot").AddBlocks(
		Section().SetText(Text(MRKDWN, B(":sunny: Good Morning ", time.Now().Format("02/01/2006"), " :spinthink:"))),
	)
	err := getNews(msg)
	if err != nil {
		return "", err
	}
	err = getQuote(msg)
	if err != nil {
		return "", err
	}
	return msg.Send()

}
