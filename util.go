package main

import (
	"io/ioutil"
	"net/http"
	. "slack-bot/bot"
)


var bot = Bot().SetWebhookURL("https://hooks.slack.com/services/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")

func get(url string) ([]byte, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)

}
