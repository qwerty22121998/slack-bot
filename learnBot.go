package main

import (
	"encoding/json"
	"log"
	. "slack-bot/bot"
)

const GirlURL = "https://begirls.netlify.app/.netlify/functions/girl"

type Girl struct {
	Image string `json:"image"`
}

func getGirl() (*Girl, error) {
	girlJson, err := get(GirlURL)
	if err != nil {
		return nil, err
	}
	girl := new(Girl)
	err = json.Unmarshal(girlJson, girl)
	return girl, err
}

func learnBot() (string, error) {
	girl, err := getGirl()
	if err != nil {
		log.Fatal(err)
	}
	return bot.
		Create(nil).SetChannel("test-bot").
		AddBlocks(
			Section().SetText(Text(MRKDWN, B("Vào lớp thôi các bạn trẻ làng Đo Đo ơi :heart:"))),
			ImageB().SetTitle(":love_letter:").SetImage(girl.Image, "..."),
		).
		AddAttachment(
			Attachment().AddBlocks(
				Section().SetText(Text(MRKDWN, B("Thời gian tự học"))),
				Context().AddElements(
					Text(MRKDWN, I(":watch: 22h00 - 23h00 T2-T6")),
					),
			),
		).
		Send()
}
