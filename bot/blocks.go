package bot

type BlockType string

type IBlock interface {
	setType(t BlockType) *Block
	SetID(id string) *Block
}

type Block struct {
	Type    BlockType `json:"type"`
	BlockId string    `json:"block_id,omitempty"`
}

func (b *Block) setType(t BlockType) *Block {
	b.Type = t
	return b
}

func (b *Block) SetID(id string) *Block {
	b.BlockId = id
	return b
}

const SECTION BlockType = "section"

type SectionBlock struct {
	Block
	Text      TextObject   `json:"text"`
	Fields    []TextObject `json:"fields,omitempty"`
	Accessory iElement     `json:"accessory,omitempty"`
}

func (sb *SectionBlock) SetText(t TextObject) *SectionBlock {
	sb.Text = t
	return sb
}

func (sb *SectionBlock) AddFields(t ...TextObject) *SectionBlock {
	sb.Fields = append(sb.Fields, t...)
	return sb
}

func (sb *SectionBlock) SetAccessory(element iElement) *SectionBlock {
	sb.Accessory = element
	return sb
}

func Section() *SectionBlock {
	s := new(SectionBlock)
	s.setType(SECTION)
	return s
}

const DIVIDER BlockType = "divider"

func Divider() *Block {
	s := new(Block)
	s.setType(DIVIDER)
	return s
}

const IMAGE_BLOCK BlockType = "image"

type ImageBlock struct {
	Block
	ImageUrl string     `json:"image_url"`
	AltText  string     `json:"alt_text"`
	Title    *TextObject `json:"title,omitempty"`
}

func (ib *ImageBlock) SetImage(url, alt string) *ImageBlock {
	return ib.SetImageUrl(url).SetAltText(alt)
}

func (ib *ImageBlock) SetImageUrl(url string) *ImageBlock {
	ib.ImageUrl = url
	return ib
}

func (ib *ImageBlock) SetAltText(text string) *ImageBlock {
	ib.AltText = text
	return ib
}

func (ib *ImageBlock) SetTitle(text string) *ImageBlock {
	txt := Text(PLAIN, text)
	ib.Title = &txt
	return ib
}

func ImageB() *ImageBlock {
	i := new(ImageBlock)
	i.setType(IMAGE_BLOCK)
	return i
}

const ACTION BlockType = "actions"

type ActionsBlock struct {
	Block
	Elements []iElement `json:"elements"`
}

func (ab *ActionsBlock) AddElements(e ...iElement) *ActionsBlock {
	ab.Elements = append(ab.Elements, e...)
	return ab
}

func Actions() *ActionsBlock {
	a := new(ActionsBlock)
	a.setType(ACTION)
	return a
}

const CONTEXT BlockType = "context"

type ContextBlock struct {
	Block
	Elements []interface{} `json:"elements"`
}

func (cb *ContextBlock) AddElements(e ...interface{}) *ContextBlock {
	cb.Elements = append(cb.Elements, e...)
	return cb
}

func Context() *ContextBlock {
	c := new(ContextBlock)
	c.setType(CONTEXT)
	return c
}

const INPUT BlockType = "input"

type InputBlock struct {
	Block
	Label    TextObject `json:"label"`
	Element  iElement   `json:"element"`
	Hint     TextObject `json:"hint,omitempty"`
	Optional bool       `json:"optional,omitempty"`
}

func (ib *InputBlock) SetLabel(text string) *InputBlock {
	ib.Label = Text(PLAIN, text)
	return ib
}

func (ib *InputBlock) SetElement(e iElement) *InputBlock {
	ib.Element = e
	return ib
}

func (ib *InputBlock) SetHint(text string) *InputBlock {
	ib.Hint = Text(PLAIN, text)
	return ib
}

func (ib *InputBlock) SetOptional(o bool) *InputBlock {
	ib.Optional = o
	return ib
}

func Input() *InputBlock {
	i := new(InputBlock)
	i.setType(INPUT)
	return i
}

const FILE BlockType = "file"

type FileBlock struct {
	Block
	ExternalID string `json:"external_id"`
	Source     string `json:"source"`
}

func (fb *FileBlock) SetExternalID(id string) *FileBlock {
	fb.ExternalID = id
	return fb
}
func (fb *FileBlock) SetSource(source string) *FileBlock {
	fb.Source = source
	return fb
}
