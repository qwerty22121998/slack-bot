package bot

type iElement interface {
	setType(t ElementType) *Element
}

type ElementType string

const BUTTON = "button"

type Element struct {
	Type ElementType `json:"type"`
}

func (e *Element) setType(t ElementType) *Element {
	e.Type = t
	return e
}

type ActionElement struct {
	Element
	ActionId string         `json:"action_id"`
	Confirm  *ConfirmObject `json:"confirm,omitempty"`
}

func (ae *ActionElement) SetActionId(id string) *ActionElement {
	ae.ActionId = id
	return ae
}

func (ae *ActionElement) SetConfirm(c *ConfirmObject) *ActionElement {
	ae.Confirm = c
	return ae
}

type ButtonElement struct {
	ActionElement
	Text  TextObject  `json:"text"`
	Url   string      `json:"url,omitempty"`
	Value string      `json:"value,omitempty"`
	Style ColorScheme `json:"style,omitempty"`
}

func Button(actionId string) *ButtonElement {
	be := new(ButtonElement)
	be.SetActionId(actionId).setType(BUTTON)
	return be
}

func (be *ButtonElement) SetText(text string) *ButtonElement {
	be.Text = Text(PLAIN, text)
	return be
}

func (be *ButtonElement) SetUrl(url string) *ButtonElement {
	be.Url = url
	return be
}
func (be *ButtonElement) SetValue(v string) *ButtonElement {
	be.Value = v
	return be
}
func (be *ButtonElement) SetStyle(s ColorScheme) *ButtonElement {
	be.Style = s
	return be
}

type CheckboxGroupsElement struct {
	ActionElement
	Options        []OptionObject `json:"options"`
	InitialOptions []OptionObject `json:"initial_options,omitempty"`
}

func (cge *CheckboxGroupsElement) AddOptions(o ...OptionObject) *CheckboxGroupsElement {
	cge.Options = append(cge.Options, o...)
	return cge
}

func (cge *CheckboxGroupsElement) AddInitialOptions(o ...OptionObject) *CheckboxGroupsElement {
	cge.InitialOptions = append(cge.InitialOptions, o...)
	return cge
}

type DatePickerElement struct {
	ActionElement
	Placeholder TextObject `json:"placeholder,omitempty"`
	InitialDate string     `json:"initial_date,omitempty"`
}

func (dpe *DatePickerElement) SetPlaceholder(text string) *DatePickerElement {
	dpe.Placeholder = Text(PLAIN, text)
	return dpe
}
func (dpe *DatePickerElement) SetInitialDate(date string) *DatePickerElement {
	dpe.InitialDate = date
	return dpe
}

const IMAGE ElementType = "image"

type ImageElement struct {
	Element
	ImageUrl string `json:"image_url"`
	AltText  string `json:"alt_text"`
}

func Image(url, alt string) *ImageElement {
	img := new(ImageElement)
	img.setType(IMAGE)
	return img.SetImageUrl(url).SetAltText(alt)
}

func (ie *ImageElement) SetImageUrl(url string) *ImageElement {
	ie.ImageUrl = url
	return ie
}

func (ie *ImageElement) SetAltText(text string) *ImageElement {
	ie.AltText = text
	return ie
}

const MULTISELECT_STATIC = "multi_static_select"

type MultiSelectStaticElement struct {
	ActionElement
	Placeholder      TextObject     `json:"placeholder"`
	Options          []OptionObject `json:"options"`
	OptionGroups     []OptionGroup  `json:"option_groups,omitempty"`
	InitialOptions   []OptionObject `json:"initial_options,omitempty"`
	MaxSelectedItems int            `json:"max_selected_items,omitempty"`
}

func MultiselectStatic() *MultiSelectStaticElement {
	m := new(MultiSelectStaticElement)
	m.setType(MULTISELECT_STATIC)
	return m
}

func (m *MultiSelectStaticElement) SetPlaceholder(text string) *MultiSelectStaticElement {
	m.Placeholder = Text(PLAIN, text)
	return m
}
func (m *MultiSelectStaticElement) AddOptions(oo ...OptionObject) *MultiSelectStaticElement {
	m.Options = append(m.Options, oo...)
	return m
}
func (m *MultiSelectStaticElement) AddOptionGroups(og ...OptionGroup) *MultiSelectStaticElement {
	m.OptionGroups = append(m.OptionGroups, og...)
	return m
}
func (m *MultiSelectStaticElement) AddInitialOptions(oo ...OptionObject) *MultiSelectStaticElement {
	m.InitialOptions = append(m.InitialOptions, oo...)
	return m
}
func (m *MultiSelectStaticElement) SetMaxSelectedItems(max int) *MultiSelectStaticElement {
	m.MaxSelectedItems = max
	return m
}

// TODO : Multiselect external, userlist, conversation list, channel list

// TODO : https://api.slack.com/reference/block-kit/block-elements#overflow

// TODO : https://api.slack.com/reference/block-kit/block-elements#input

// TODO : https://api.slack.com/reference/block-kit/block-elements#radio

// TODO : https://api.slack.com/reference/block-kit/block-elements#static_select
