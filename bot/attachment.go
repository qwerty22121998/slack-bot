package bot

type IAttachment interface {
	isAttachment()
}

type MessageAttachment struct {
	Blocks     []IBlock    `json:"blocks,omitempty"`
	Color      ColorScheme `json:"color,omitempty"`
	AuthorIcon string      `json:"author_icon,omitempty"`
	AuthorLink string      `json:"author_link,omitempty"`
	AuthorName string      `json:"author_name,omitempty"`
	Footer     string      `json:"footer,omitempty"`
	FooterIcon string      `json:"footer_icon,omitempty"`
	ImageUrl   string      `json:"image_url,omitempty"`
	MrkdwnIn   []string    `json:"mrkdwn_in,omitempty"`
	Pretext    string      `json:"pretext,omitempty"`
	Text       string      `json:"text,omitempty"`
	ThumbUrl   string      `json:"thumb_url,omitempty"`
	Title      string      `json:"title,omitempty"`
	TitleLink  string      `json:"title_link,omitempty"`
	Ts         int64      `json:"ts,omitempty"`
}

func (a *MessageAttachment) isAttachment() {

}

func Attachment() *MessageAttachment {
	return new(MessageAttachment)
}

func (a *MessageAttachment) AddBlocks(blocks ...IBlock) *MessageAttachment {
	a.Blocks = append(a.Blocks, blocks...)
	return a
}

func (a *MessageAttachment) SetColor(c ColorScheme) *MessageAttachment {
	a.Color = c
	return a
}

func (a *MessageAttachment) SetAuthorIcon(text string) *MessageAttachment {
	a.AuthorIcon = text
	return a
}
func (a *MessageAttachment) SetAuthorLink(text string) *MessageAttachment {
	a.AuthorLink = text
	return a
}
func (a *MessageAttachment) SetAuthorName(text string) *MessageAttachment {
	a.AuthorName = text
	return a
}
func (a *MessageAttachment) SetFooter(text string) *MessageAttachment {
	a.Footer = text
	return a
}
func (a *MessageAttachment) SetFooterIcon(text string) *MessageAttachment {
	a.FooterIcon = text
	return a
}
func (a *MessageAttachment) SetImageUrl(text string) *MessageAttachment {
	a.ImageUrl = text
	return a
}
func (a *MessageAttachment) AddMrkdwnIn(text ...string) *MessageAttachment {
	a.MrkdwnIn = append(a.MrkdwnIn, text...)
	return a
}
func (a *MessageAttachment) SetPretext(text string) *MessageAttachment {
	a.Pretext = text
	return a
}
func (a *MessageAttachment) SetText(text string) *MessageAttachment {
	a.Text = text
	return a
}
func (a *MessageAttachment) SetThumbUrl(text string) *MessageAttachment {
	a.ThumbUrl = text
	return a
}
func (a *MessageAttachment) SetTitle(text string) *MessageAttachment {
	a.Title = text
	return a
}
func (a *MessageAttachment) SetTitleLink(text string) *MessageAttachment {
	a.TitleLink = text
	return a
}
func (a *MessageAttachment) SetTs(ts int64) *MessageAttachment {
	a.Ts = ts
	return a
}
