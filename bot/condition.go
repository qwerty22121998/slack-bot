package bot

type Condition func(i interface{}) bool

var NULL Condition = func(i interface{}) bool {
	switch i.(type) {
	case int:
		return i == 0
	case string:
		return i == ""
	default:
		return i == nil
	}
}

func If(condition Condition, value, trueValue, falseValue interface{}) interface{} {
	if condition(value) {
		return trueValue
	}
	return falseValue
}
