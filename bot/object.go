package bot

import "strings"

type TextType string

const PLAIN TextType = "plain_text"
const MRKDWN TextType = "mrkdwn"

type iOject interface {
	isObject()
}

type CompositionObject struct{}

func (CompositionObject) isObject() {}

type TextObject struct {
	CompositionObject
	Type     TextType `json:"type"`
	Text     string   `json:"text"`
	Emoji    bool     `json:"emoji,omitempty"`
	Verbatim bool     `json:"verbatim,omitempty"`
}

func Text(t TextType, texts ...string) TextObject {
	to := new(TextObject)
	to.setType(t)
	text := strings.Join(texts, "\n")
	to.SetText(text)
	return *to
}

func (te *TextObject) setType(t TextType) *TextObject {
	te.Type = t
	return te
}

func (te *TextObject) SetText(t string) *TextObject {
	te.Text = t
	return te
}

func (te *TextObject) SetEmoji(e bool) *TextObject {
	te.Emoji = e
	return te
}

func (te *TextObject) SetVerbatim(v bool) *TextObject {
	te.Verbatim = v
	return te
}

type ColorScheme string

const PRIMARY ColorScheme = "primary"
const DANGER ColorScheme = "danger"

type ConfirmObject struct {
	CompositionObject
	Title   TextObject  `json:"title"`
	Text    TextObject  `json:"text"`
	Confirm TextObject  `json:"confirm"`
	Deny    TextObject  `json:"deny"`
	Style   ColorScheme `json:"style,omitempty"`
}

func Confirm() *ConfirmObject {
	return new(ConfirmObject)
}

func (ce *ConfirmObject) SetTitle(text string) *ConfirmObject {
	ce.Title = Text(PLAIN, text)
	return ce
}
func (ce *ConfirmObject) SetText(t TextType, text string) *ConfirmObject {
	ce.Text = Text(t, text)
	return ce
}
func (ce *ConfirmObject) SetConfirm(text string) *ConfirmObject {
	ce.Confirm = Text(PLAIN, text)
	return ce
}
func (ce *ConfirmObject) SetDeny(text string) *ConfirmObject {
	ce.Deny = Text(PLAIN, text)
	return ce
}
func (ce *ConfirmObject) SetStyle(s ColorScheme) *ConfirmObject {
	ce.Style = s
	return ce
}

type OptionObject struct {
	CompositionObject
	Text        TextObject `json:"text"`
	Value       string     `json:"value"`
	Description TextObject `json:"description,omitempty"`
	Url         string     `json:"url,omitempty"`
}

func (oo *OptionObject) SetText(t TextType, text string) *OptionObject {
	oo.Text = Text(t, text)
	return oo
}
func (oo *OptionObject) SetValue(v string) *OptionObject {
	oo.Value = v
	return oo
}
func (oo *OptionObject) SetDescription(text string) *OptionObject {
	oo.Description = Text(PLAIN, text)
	return oo
}
func (oo *OptionObject) SetUrl(u string) *OptionObject {
	oo.Url = u
	return oo
}

type OptionGroup struct {
	Label   TextObject     `json:"label"`
	Options []OptionObject `json:"options"`
}

func (og *OptionGroup) SetLabel(text string) *OptionGroup {
	og.Label = Text(PLAIN, text)
	return og
}

func (og *OptionGroup) AddOptions(oo ...OptionObject) *OptionGroup {
	og.Options = append(og.Options, oo...)
	return og
}

type FilterObject struct {
	CompositionObject
	Include                       []string `json:"include,omitempty"`
	ExcludeExternalSharedChannels bool     `json:"exclude_external_shared_channels,omitempty"`
	ExcludeBotUsers               bool     `json:"exclude_bot_users,omitempty"`
}
