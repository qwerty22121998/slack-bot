package bot

import (
	"fmt"
	"strings"
)

func modify(begin string, end string, txt ...interface{}) string {
	return fmt.Sprintf("%v%v%v", begin, fmt.Sprint(txt...), end)
}

func Link(url string, txt ...interface{}) string {
	return fmt.Sprintf("<%v|%v>", url, fmt.Sprint(txt...))
}

func I(txt ...interface{}) string {
	return modify("_", "_", txt...)
}

func B(txt ...interface{}) string {
	return modify("*", "*", txt...)
}

func Strike(txt ...interface{}) string {
	return modify("~", "~", txt...)
}

func Quote(txt ...interface{}) string {
	return modify(">", "", txt...)
}

func Code(txt ...interface{}) string {
	return modify("```", "```", txt...)
}

func List(txt ...interface{}) string {
	var list []string
	for _, v := range txt {
		list = append(list, modify("- ", "", v))
	}
	return strings.Join(list, "")
}

func Mention(uid string) string {
	return modify("<@", ">", uid)
}

type SpecialMention string

const HERE SpecialMention = "!here"
const CHANNEL SpecialMention = "!channel"
const EVERYONE SpecialMention = "!everyone"

func MentionSpecial(m SpecialMention) string {
	return modify("<!", ">", m)
}
