package bot

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type Message struct {
	SlackBot
	Text        string        `json:"text,omitempty"`
	Channel     string        `json:"channel,omitempty"`
	Blocks      []IBlock      `json:"blocks,omitempty"`
	Attachments []IAttachment `json:"attachments,omitempty"`
}

func (m *Message) SetText(text string) *Message {
	m.Text = text
	return m
}

func (m *Message) SetChannel(channel string) *Message {
	m.Channel = channel
	return m
}

func (m *Message) AddBlocks(blocks ...IBlock) *Message {
	m.Blocks = append(m.Blocks, blocks...)
	return m
}

func (m *Message) AddAttachment(attachments ...IAttachment) *Message {
	m.Attachments = append(m.Attachments, attachments...)
	return m
}

func (m *Message) Send() (string, error) {
	jsonObject, _ := json.Marshal(m)
	res, err := http.Post(m.WebhookURL, "application/json", bytes.NewReader(jsonObject))
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	return string(body), err
}
