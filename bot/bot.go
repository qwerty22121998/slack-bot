package bot

type SlackBot struct {
	WebhookURL string `json:"-"`
	Username   string `json:"username,omitempty"`
	IconUrl    string `json:"icon_url,omitempty"`
}

func (b SlackBot) Create(msg *Message) *Message {
	if msg == nil {
		msg = new(Message)
	}
	msg.SlackBot = b
	return msg
}
func (b *SlackBot) SetWebhookURL(url string) *SlackBot {
	b.WebhookURL = url
	return b
}

func (b *SlackBot) SetUsername(username string) *SlackBot {
	b.Username = username
	return b
}

func (b *SlackBot) SetIcon(url string) *SlackBot {
	b.IconUrl = url
	return b
}

func Bot() *SlackBot {
	return new(SlackBot)
}

